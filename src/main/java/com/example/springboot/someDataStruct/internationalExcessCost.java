package com.example.springboot.someDataStruct;

import com.example.springboot.OrdinaryLuggage;
import com.example.springboot.SpecialLuggage;

import java.util.ArrayList;

public class internationalExcessCost {
    public OrdinaryLuggage[] ordinaryLuggage;
    public SpecialLuggage[] specialLuggage;
    public freeCheckedBaggage freeCheckedBaggage;

    public void setValue(OrdinaryLuggage[] ord,SpecialLuggage[] spe,freeCheckedBaggage free){
        this.ordinaryLuggage=ord;
        this.specialLuggage=spe;
        this.freeCheckedBaggage=free;
    }

    //计算区域1国际旅客行李费用
    public int area1(int maxWeight){
        int result=0;//结果
        int baggageSize=0;//行李尺寸
        boolean[] isAdd=new boolean[this.ordinaryLuggage.length];//记录此行李是否被计算
        if(maxWeight==32) {//如果免费行李重量为32KG
            if (this.ordinaryLuggage != null) {
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历行李数组,查看有没有不超尺寸且不超重量的行李，并加入免费托运行李额
                    baggageSize = ordinaryLuggage[i].getLen() + ordinaryLuggage[i].getWidth() + ordinaryLuggage[i].getHeight();
                    if (this.freeCheckedBaggage.number == 0) break;//如果免费托运行李额度用满就推出
                    if (!isAdd[i]) {
                        if (baggageSize <= 158) {//不超尺寸就加入
                            isAdd[i] = true;//标记该行李
                            freeCheckedBaggage.number--;//免费托运行李额度减一
                        }
                    }
                }
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历剩下的行李，计算费用
                    baggageSize = ordinaryLuggage[i].getLen() + ordinaryLuggage[i].getWidth() + ordinaryLuggage[i].getHeight();
                    if (!isAdd[i]) {
                        if (this.freeCheckedBaggage.number > 0) {
                            if (baggageSize <= 158) {
                                isAdd[i] = true;//标记该行李
                                this.freeCheckedBaggage.number--;//免费托运行李额度减一
                            } else {
                                result += 980;
                                isAdd[i] = true;//标记该行李
                                this.freeCheckedBaggage.number--;//免费托运行李额度减一
                            }
                        } else {
                            if (this.freeCheckedBaggage.number == 0) {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 1400;
                                } else {
                                    isAdd[i] = true;
                                    result += 1400 + 980;
                                }
                                this.freeCheckedBaggage.number--;
                            } else if (this.freeCheckedBaggage.number == -1) {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 2000;
                                } else {
                                    isAdd[i] = true;
                                    result += 2000 + 980;
                                }
                                this.freeCheckedBaggage.number--;
                            } else {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 3000;
                                } else {
                                    isAdd[i] = true;
                                    result += 3000 + 980;
                                }
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    }
                }
            }

            //特殊行李计费
            if (this.specialLuggage != null) {
                for (int i = 0; i < this.specialLuggage.length; i++) {
                    if (this.specialLuggage[i].getLuggageType().equals("类别2") || this.specialLuggage[i].getLuggageType().equals("类别5")) {//计入免费行李额度的类别
                        if (this.freeCheckedBaggage.number > 0) {//因为不考虑尺寸，直接不计费用
                            this.freeCheckedBaggage.number--;
                        } else {//超额第一件
                            if (this.freeCheckedBaggage.number == 0) {
                                result += 1400;
                                this.freeCheckedBaggage.number--;
                            } else if (this.freeCheckedBaggage.number == -1) {//超额第二件
                                result += 2000;
                                this.freeCheckedBaggage.number--;
                            } else {//超额第三件
                                result += 3000;
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    } else {//不计入免费行李额度
                        result += getSpecialCost(this.specialLuggage[i].getLuggageType(), this.specialLuggage[i].getWeight());
                    }
                }
            }
        }

        else if(maxWeight==23) {//免费行李最大重量为23KG
            if (this.ordinaryLuggage != null) {
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历行李数组，先将既不超尺寸又不超重量的行李添加
                    baggageSize = this.ordinaryLuggage[i].getLen() + this.ordinaryLuggage[i].getWidth() + this.ordinaryLuggage[i].getHeight();
                    if (this.freeCheckedBaggage.number <= 0) break;
                    if (!isAdd[i]) {
                        if (this.ordinaryLuggage[i].getWeight() <= 23 && baggageSize <= 158) {
                            isAdd[i] = true;
                            this.freeCheckedBaggage.number--;
                        }
                    }
                }

                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历剩余行李
                    baggageSize = this.ordinaryLuggage[i].getLen() + this.ordinaryLuggage[i].getWidth() + this.ordinaryLuggage[i].getHeight();
                    if (!isAdd[i]) {
                        if (this.freeCheckedBaggage.number > 0) {
                            result += getMoneyInArea1(baggageSize, this.ordinaryLuggage[i].getWeight());
                            isAdd[i] = true;
                            this.freeCheckedBaggage.number--;
                        } else {
                            if (this.freeCheckedBaggage.number == 0) {
                                result += 1400 + getMoneyInArea1(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            } else if (this.freeCheckedBaggage.number == -1) {
                                result += 2000 + getMoneyInArea1(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            } else {
                                result += 3000 + getMoneyInArea1(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            }
                        }
                    }
                }
            }

            if (this.specialLuggage != null) {
                for (int i = 0; i < this.specialLuggage.length; i++) {//遍历特殊行李
                    if (this.specialLuggage[i].getLuggageType().equals("类别2") || this.specialLuggage[i].getLuggageType().equals("类别5")) {//计入免费行李额度的类别
                        if (this.freeCheckedBaggage.number > 0) {//因为不考虑尺寸，直接不计费用
                            if (this.ordinaryLuggage[i].getWeight() <= 28 && this.ordinaryLuggage[i].getWeight() > 23)
                                result += 380;
                            if (this.ordinaryLuggage[i].getWeight() > 28 && this.ordinaryLuggage[i].getWeight() <= 32)
                                result += 980;
                            this.freeCheckedBaggage.number--;
                        } else {//超额第一件
                            if (this.freeCheckedBaggage.number == 0) {
                                result += 1400 + getMoneyInArea1(100, this.ordinaryLuggage[i].getWeight());
                                this.freeCheckedBaggage.number--;
                            } else if (this.freeCheckedBaggage.number == -1) {//超额第二件
                                result += 2000 + getMoneyInArea1(100, this.ordinaryLuggage[i].getWeight());
                                ;
                                this.freeCheckedBaggage.number--;
                            } else {//超额第三件
                                result += 3000 + getMoneyInArea1(100, this.ordinaryLuggage[i].getWeight());
                                ;
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    } else {//不计入免费行李额度
                        result += getSpecialCost(this.specialLuggage[i].getLuggageType(), this.specialLuggage[i].getWeight());
                    }
                }
            }
        }
        return result;
    }

    //计算区域2国际旅客行李费用
    public int area2(int maxWeight){
        int result=0;//结果
        int baggageSize=0;//行李尺寸
        boolean[] isAdd=new boolean[this.ordinaryLuggage.length];//记录此行李是否被计算
        if(maxWeight==32) {//如果免费行李重量为32KG
            if (this.ordinaryLuggage != null) {
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历行李数组,查看有没有不超尺寸且不超重量的行李，并加入免费托运行李额
                    baggageSize = ordinaryLuggage[i].getLen() + ordinaryLuggage[i].getWidth() + ordinaryLuggage[i].getHeight();
                    if (this.freeCheckedBaggage.number == 0) break;//如果免费托运行李额度用满就跳出
                    if (!isAdd[i]) {
                        if (baggageSize <= 158) {//不超尺寸就加入
                            isAdd[i] = true;//标记该行李
                            freeCheckedBaggage.number--;//免费托运行李额度减一
                        }
                    }
                }
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历剩下的行李，计算费用
                    baggageSize = ordinaryLuggage[i].getLen() + ordinaryLuggage[i].getWidth() + ordinaryLuggage[i].getHeight();
                    if (!isAdd[i]) {
                        if (this.freeCheckedBaggage.number > 0) {
                            if (baggageSize <= 158) {
                                isAdd[i] = true;//标记该行李
                                this.freeCheckedBaggage.number--;//免费托运行李额度减一
                            } else {
                                result += 690;
                                isAdd[i] = true;//标记该行李
                                this.freeCheckedBaggage.number--;//免费托运行李额度减一
                            }
                        } else {
                            if (this.freeCheckedBaggage.number >= -1) {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 1100;
                                } else {
                                    isAdd[i] = true;
                                    result += 1100 + 690;
                                }
                                this.freeCheckedBaggage.number--;
                            } else {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 1590;
                                } else {
                                    isAdd[i] = true;
                                    result += 1590 + 690;
                                }
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    }
                }
            }

            //特殊行李计费
            if (this.specialLuggage != null) {
                for (int i = 0; i < this.specialLuggage.length; i++) {
                    if (this.specialLuggage[i].getLuggageType().equals("类别2") || this.specialLuggage[i].getLuggageType().equals("类别5")) {//计入免费行李额度的类别
                        if (this.freeCheckedBaggage.number > 0) {//因为不考虑尺寸，直接不计费用
                            this.freeCheckedBaggage.number--;
                        } else {//超额第一件
                            if (this.freeCheckedBaggage.number >= -1) {
                                result += 1100;
                                this.freeCheckedBaggage.number--;
                            } else {//超额第三件
                                result += 1590;
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    } else {//不计入免费行李额度
                        result += getSpecialCost(this.specialLuggage[i].getLuggageType(), this.specialLuggage[i].getWeight());
                    }
                }
            }
        }

        else if(maxWeight==23) {//免费行李最大重量为23KG
            if (this.ordinaryLuggage != null) {
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历行李数组，先将既不超尺寸又不超重量的行李添加
                    baggageSize = this.ordinaryLuggage[i].getLen() + this.ordinaryLuggage[i].getWidth() + this.ordinaryLuggage[i].getHeight();
                    if (this.freeCheckedBaggage.number <= 0) break;
                    if (!isAdd[i]) {
                        if (this.ordinaryLuggage[i].getWeight() <= 23 && baggageSize <= 158) {
                            isAdd[i] = true;
                            this.freeCheckedBaggage.number--;
                        }
                    }
                }

                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历剩余行李
                    baggageSize = this.ordinaryLuggage[i].getLen() + this.ordinaryLuggage[i].getWidth() + this.ordinaryLuggage[i].getHeight();
                    if (!isAdd[i]) {
                        if (this.freeCheckedBaggage.number > 0) {
                            result += getMoneyInArea2(baggageSize, this.ordinaryLuggage[i].getWeight());
                            isAdd[i] = true;
                            this.freeCheckedBaggage.number--;
                        } else {
                            if (this.freeCheckedBaggage.number >= -1) {
                                result += 1100 + getMoneyInArea2(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            } else {
                                result += 1590 + getMoneyInArea2(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            }
                        }
                    }
                }
            }

            if (this.specialLuggage != null) {
                for (int i = 0; i < this.specialLuggage.length; i++) {//遍历特殊行李
                    if (this.specialLuggage[i].getLuggageType().equals("类别2") || this.specialLuggage[i].getLuggageType().equals("类别5")) {//计入免费行李额度的类别
                        if (this.freeCheckedBaggage.number > 0) {//因为不考虑尺寸，直接不计费用
                            if (this.ordinaryLuggage[i].getWeight() <= 28 && this.ordinaryLuggage[i].getWeight() > 23)
                                result += 280;
                            if (this.ordinaryLuggage[i].getWeight() > 28 && this.ordinaryLuggage[i].getWeight() <= 32)
                                result += 690;
                            this.freeCheckedBaggage.number--;
                        } else {//超额第一件/第二件
                            if (this.freeCheckedBaggage.number >= -1) {
                                result += 1100 + getMoneyInArea2(100, this.ordinaryLuggage[i].getWeight());
                                this.freeCheckedBaggage.number--;
                            } else {//超额第三件
                                result += 1590 + getMoneyInArea2(100, this.ordinaryLuggage[i].getWeight());
                                ;
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    } else {//不计入免费行李额度
                        result += getSpecialCost(this.specialLuggage[i].getLuggageType(), this.specialLuggage[i].getWeight());
                    }
                }
            }
        }
        return result;
    }


    //计算区域3国际旅客行李费用
    public int area3(int maxWeight){
        int result=0;//结果
        int baggageSize=0;//行李尺寸
        boolean[] isAdd=new boolean[this.ordinaryLuggage.length];//记录此行李是否被计算
        if(maxWeight==32) {//如果免费行李重量为32KG
            if (this.ordinaryLuggage != null) {
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历行李数组,查看有没有不超尺寸且不超重量的行李，并加入免费托运行李额
                    baggageSize = ordinaryLuggage[i].getLen() + ordinaryLuggage[i].getWidth() + ordinaryLuggage[i].getHeight();
                    if (this.freeCheckedBaggage.number == 0) break;//如果免费托运行李额度用满就跳出
                    if (!isAdd[i]) {
                        if (baggageSize <= 158) {//不超尺寸就加入
                            isAdd[i] = true;//标记该行李
                            freeCheckedBaggage.number--;//免费托运行李额度减一
                        }
                    }
                }
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历剩下的行李，计算费用
                    baggageSize = ordinaryLuggage[i].getLen() + ordinaryLuggage[i].getWidth() + ordinaryLuggage[i].getHeight();
                    if (!isAdd[i]) {
                        if (this.freeCheckedBaggage.number > 0) {
                            if (baggageSize <= 158) {
                                isAdd[i] = true;//标记该行李
                                this.freeCheckedBaggage.number--;//免费托运行李额度减一
                            } else {
                                result += 520;
                                isAdd[i] = true;//标记该行李
                                this.freeCheckedBaggage.number--;//免费托运行李额度减一
                            }
                        } else {
                            if (this.freeCheckedBaggage.number >= -1) {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 1170;
                                } else {
                                    isAdd[i] = true;
                                    result += 1170 + 520;
                                }
                                this.freeCheckedBaggage.number--;
                            } else {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 1590;
                                } else {
                                    isAdd[i] = true;
                                    result += 1590 + 520;
                                }
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    }
                }
            }
            //特殊行李计费
            if (this.specialLuggage != null) {
                for (int i = 0; i < this.specialLuggage.length; i++) {
                    if (this.specialLuggage[i].getLuggageType().equals("类别2") || this.specialLuggage[i].getLuggageType().equals("类别5")) {//计入免费行李额度的类别
                        if (this.freeCheckedBaggage.number > 0) {//因为不考虑尺寸，直接不计费用
                            this.freeCheckedBaggage.number--;
                        } else {//超额第一件
                            if (this.freeCheckedBaggage.number >= -1) {
                                result += 1170;
                                this.freeCheckedBaggage.number--;
                            } else {//超额第三件
                                result += 1590;
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    } else {//不计入免费行李额度
                        result += getSpecialCost(this.specialLuggage[i].getLuggageType(), this.specialLuggage[i].getWeight());
                    }
                }
            }
        }

        else if(maxWeight==23) {//免费行李最大重量为23KG
            if (this.ordinaryLuggage != null) {
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历行李数组，先将既不超尺寸又不超重量的行李添加
                    baggageSize = this.ordinaryLuggage[i].getLen() + this.ordinaryLuggage[i].getWidth() + this.ordinaryLuggage[i].getHeight();
                    if (this.freeCheckedBaggage.number <= 0) break;
                    if (!isAdd[i]) {
                        if (this.ordinaryLuggage[i].getWeight() <= 23 && baggageSize <= 158) {
                            isAdd[i] = true;
                            this.freeCheckedBaggage.number--;
                        }
                    }
                }

                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历剩余行李
                    baggageSize = this.ordinaryLuggage[i].getLen() + this.ordinaryLuggage[i].getWidth() + this.ordinaryLuggage[i].getHeight();
                    if (!isAdd[i]) {
                        if (this.freeCheckedBaggage.number > 0) {
                            result += getMoneyInArea3(baggageSize, this.ordinaryLuggage[i].getWeight());
                            isAdd[i] = true;
                            this.freeCheckedBaggage.number--;
                        } else {
                            if (this.freeCheckedBaggage.number >= -1) {
                                result += 1170 + getMoneyInArea3(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            } else {
                                result += 1590 + getMoneyInArea3(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            }
                        }
                    }
                }
            }


            if (this.specialLuggage != null) {
                for (int i = 0; i < this.specialLuggage.length; i++) {//遍历特殊行李
                    if (this.specialLuggage[i].getLuggageType().equals("类别2") || this.specialLuggage[i].getLuggageType().equals("类别5")) {//计入免费行李额度的类别
                        if (this.freeCheckedBaggage.number > 0) {//因为不考虑尺寸，直接不计费用
                            if (this.ordinaryLuggage[i].getWeight() <= 32 && this.ordinaryLuggage[i].getWeight() > 23)
                                result += 520;
                            this.freeCheckedBaggage.number--;
                        } else {//超额第一件/第二件
                            if (this.freeCheckedBaggage.number >= -1) {
                                result += 1170 + getMoneyInArea3(100, this.ordinaryLuggage[i].getWeight());
                                this.freeCheckedBaggage.number--;
                            } else {//超额第三件
                                result += 1590 + getMoneyInArea3(100, this.ordinaryLuggage[i].getWeight());
                                ;
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    } else {//不计入免费行李额度
                        result += getSpecialCost(this.specialLuggage[i].getLuggageType(), this.specialLuggage[i].getWeight());
                    }
                }
            }
        }
        return result;
    }

    //计算区域4国际旅客行李费用
    public int area4(int maxWeight){
        int result=0;//结果
        int baggageSize=0;//行李尺寸
        boolean[] isAdd=new boolean[this.ordinaryLuggage.length];//记录此行李是否被计算
        if(maxWeight==32) {//如果免费行李重量为32KG
            if (this.ordinaryLuggage != null) {
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历行李数组,查看有没有不超尺寸且不超重量的行李，并加入免费托运行李额
                    baggageSize = ordinaryLuggage[i].getLen() + ordinaryLuggage[i].getWidth() + ordinaryLuggage[i].getHeight();
                    if (this.freeCheckedBaggage.number == 0) break;//如果免费托运行李额度用满就跳出
                    if (!isAdd[i]) {
                        if (baggageSize <= 158) {//不超尺寸就加入
                            isAdd[i] = true;//标记该行李
                            freeCheckedBaggage.number--;//免费托运行李额度减一
                        }
                    }
                }
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历剩下的行李，计算费用
                    baggageSize = ordinaryLuggage[i].getLen() + ordinaryLuggage[i].getWidth() + ordinaryLuggage[i].getHeight();
                    if (!isAdd[i]) {
                        if (this.freeCheckedBaggage.number > 0) {
                            if (baggageSize <= 158) {
                                isAdd[i] = true;//标记该行李
                                this.freeCheckedBaggage.number--;//免费托运行李额度减一
                            } else {
                                result += 1040;
                                isAdd[i] = true;//标记该行李
                                this.freeCheckedBaggage.number--;//免费托运行李额度减一
                            }
                        } else {
                            if (this.freeCheckedBaggage.number >= -1) {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 1380;
                                } else {
                                    isAdd[i] = true;
                                    result += 1380 + 1040;
                                }
                                this.freeCheckedBaggage.number--;
                            } else {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 1590;
                                } else {
                                    isAdd[i] = true;
                                    result += 1590 + 1040;
                                }
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    }
                }
            }
            //特殊行李计费
            if (this.specialLuggage != null) {
                for (int i = 0; i < this.specialLuggage.length; i++) {
                    if (this.specialLuggage[i].getLuggageType().equals("类别2") || this.specialLuggage[i].getLuggageType().equals("类别5")) {//计入免费行李额度的类别
                        if (this.freeCheckedBaggage.number > 0) {//因为不考虑尺寸，直接不计费用
                            this.freeCheckedBaggage.number--;
                        } else {//超额第一件
                            if (this.freeCheckedBaggage.number >= -1) {
                                result += 1380;
                                this.freeCheckedBaggage.number--;
                            } else {//超额第三件
                                result += 1590;
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    } else {//不计入免费行李额度
                        result += getSpecialCost(this.specialLuggage[i].getLuggageType(), this.specialLuggage[i].getWeight());
                    }
                }
            }
        }

        else if(maxWeight==23) {//免费行李最大重量为23KG
            if (this.ordinaryLuggage != null) {
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历行李数组，先将既不超尺寸又不超重量的行李添加
                    baggageSize = this.ordinaryLuggage[i].getLen() + this.ordinaryLuggage[i].getWidth() + this.ordinaryLuggage[i].getHeight();
                    if (this.freeCheckedBaggage.number <= 0) break;
                    if (!isAdd[i]) {
                        if (this.ordinaryLuggage[i].getWeight() <= 23 && baggageSize <= 158) {
                            isAdd[i] = true;
                            this.freeCheckedBaggage.number--;
                        }
                    }
                }

                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历剩余行李
                    baggageSize = this.ordinaryLuggage[i].getLen() + this.ordinaryLuggage[i].getWidth() + this.ordinaryLuggage[i].getHeight();
                    if (!isAdd[i]) {
                        if (this.freeCheckedBaggage.number > 0) {
                            result += getMoneyInArea4(baggageSize, this.ordinaryLuggage[i].getWeight());
                            isAdd[i] = true;
                            this.freeCheckedBaggage.number--;
                        } else {
                            if (this.freeCheckedBaggage.number >= -1) {
                                result += 1380 + getMoneyInArea4(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            } else {
                                result += 1590 + getMoneyInArea4(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            }
                        }
                    }
                }
            }

            if (this.specialLuggage != null) {
                for (int i = 0; i < this.specialLuggage.length; i++) {//遍历特殊行李
                    if (this.specialLuggage[i].getLuggageType().equals("类别2") || this.specialLuggage[i].getLuggageType().equals("类别5")) {//计入免费行李额度的类别
                        if (this.freeCheckedBaggage.number > 0) {//因为不考虑尺寸
                            if (this.ordinaryLuggage[i].getWeight() <= 28 && this.ordinaryLuggage[i].getWeight() > 23)
                                result += 690;
                            if (this.ordinaryLuggage[i].getWeight() <= 32 && this.ordinaryLuggage[i].getWeight() > 28)
                                result += 1040;
                            this.freeCheckedBaggage.number--;
                        } else {//超额第一件/第二件
                            if (this.freeCheckedBaggage.number >= -1) {
                                result += 1380 + getMoneyInArea4(100, this.ordinaryLuggage[i].getWeight());
                                this.freeCheckedBaggage.number--;
                            } else {//超额第三件
                                result += 1590 + getMoneyInArea4(100, this.ordinaryLuggage[i].getWeight());
                                ;
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    } else {//不计入免费行李额度
                        result += getSpecialCost(this.specialLuggage[i].getLuggageType(), this.specialLuggage[i].getWeight());
                    }
                }
            }
        }
        return result;
    }


    //计算区域5国际旅客行李费用
    public int area5(int maxWeight){
        int result=0;//结果
        int baggageSize=0;//行李尺寸
        boolean[] isAdd=new boolean[this.ordinaryLuggage.length];//记录此行李是否被计算
        if(maxWeight==32) {//如果免费行李重量为32KG
            if (this.ordinaryLuggage != null) {
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历行李数组,查看有没有不超尺寸且不超重量的行李，并加入免费托运行李额
                    baggageSize = ordinaryLuggage[i].getLen() + ordinaryLuggage[i].getWidth() + ordinaryLuggage[i].getHeight();
                    if (this.freeCheckedBaggage.number == 0) break;//如果免费托运行李额度用满就跳出
                    if (!isAdd[i]) {
                        if (baggageSize <= 158) {//不超尺寸就加入
                            isAdd[i] = true;//标记该行李
                            freeCheckedBaggage.number--;//免费托运行李额度减一
                        }
                    }
                }
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历剩下的行李，计算费用
                    baggageSize = ordinaryLuggage[i].getLen() + ordinaryLuggage[i].getWidth() + ordinaryLuggage[i].getHeight();
                    if (!isAdd[i]) {
                        if (this.freeCheckedBaggage.number > 0) {
                            if (baggageSize <= 158) {
                                isAdd[i] = true;//标记该行李
                                this.freeCheckedBaggage.number--;//免费托运行李额度减一
                            } else {
                                result += 520;
                                isAdd[i] = true;//标记该行李
                                this.freeCheckedBaggage.number--;//免费托运行李额度减一
                            }
                        } else {
                            if (this.freeCheckedBaggage.number == 0) {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 830;
                                } else {
                                    isAdd[i] = true;
                                    result += 830 + 520;
                                }
                                this.freeCheckedBaggage.number--;
                            } else if (this.freeCheckedBaggage.number == -1) {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 1100;
                                } else {
                                    isAdd[i] = true;
                                    result += 1100 + 520;
                                }
                                this.freeCheckedBaggage.number--;
                            } else {
                                if (baggageSize <= 158) {
                                    isAdd[i] = true;//标记该行李
                                    result += 1590;
                                } else {
                                    isAdd[i] = true;
                                    result += 1590 + 520;
                                }
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    }
                }
            }

            if (this.specialLuggage != null) {
                //特殊行李计费
                for (int i = 0; i < this.specialLuggage.length; i++) {
                    if (this.specialLuggage[i].getLuggageType().equals("类别2") || this.specialLuggage[i].getLuggageType().equals("类别5")) {//计入免费行李额度的类别
                        if (this.freeCheckedBaggage.number > 0) {//因为不考虑尺寸，直接不计费用
                            this.freeCheckedBaggage.number--;
                        } else {//超额第一件
                            if (this.freeCheckedBaggage.number == 0) {
                                result += 830;
                                this.freeCheckedBaggage.number--;
                            } else if (this.freeCheckedBaggage.number == -1) {
                                result += 1100;
                                this.freeCheckedBaggage.number--;
                            } else {//超额第三件
                                result += 1590;
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    } else {//不计入免费行李额度
                        result += getSpecialCost(this.specialLuggage[i].getLuggageType(), this.specialLuggage[i].getWeight());
                    }
                }
            }
        }

        else if(maxWeight==23) {//免费行李最大重量为23KG
            if (this.ordinaryLuggage != null) {
                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历行李数组，先将既不超尺寸又不超重量的行李添加
                    baggageSize = this.ordinaryLuggage[i].getLen() + this.ordinaryLuggage[i].getWidth() + this.ordinaryLuggage[i].getHeight();
                    if (this.freeCheckedBaggage.number <= 0) break;
                    if (!isAdd[i]) {
                        if (this.ordinaryLuggage[i].getWeight() <= 23 && baggageSize <= 158) {
                            isAdd[i] = true;
                            this.freeCheckedBaggage.number--;
                        }
                    }
                }

                for (int i = 0; i < this.ordinaryLuggage.length; i++) {//遍历剩余行李
                    baggageSize = this.ordinaryLuggage[i].getLen() + this.ordinaryLuggage[i].getWidth() + this.ordinaryLuggage[i].getHeight();
                    if (!isAdd[i]) {
                        if (this.freeCheckedBaggage.number > 0) {
                            result += getMoneyInArea5(baggageSize, this.ordinaryLuggage[i].getWeight());
                            isAdd[i] = true;
                            this.freeCheckedBaggage.number--;
                        } else {
                            if (this.freeCheckedBaggage.number == 0) {
                                result += 830 + getMoneyInArea5(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            } else if (this.freeCheckedBaggage.number == -1) {
                                result += 1100 + getMoneyInArea5(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            } else {
                                result += 1590 + getMoneyInArea5(baggageSize, this.ordinaryLuggage[i].getWeight());
                                isAdd[i] = true;
                                freeCheckedBaggage.number--;
                            }
                        }
                    }
                }
            }


            if (this.specialLuggage != null) {
                for (int i = 0; i < this.specialLuggage.length; i++) {//遍历特殊行李
                    if (this.specialLuggage[i].getLuggageType().equals("类别2") || this.specialLuggage[i].getLuggageType().equals("类别5")) {//计入免费行李额度的类别
                        if (this.freeCheckedBaggage.number > 0) {//因为不考虑尺寸
                            if (this.ordinaryLuggage[i].getWeight() <= 28 && this.ordinaryLuggage[i].getWeight() > 23)
                                result += 210;
                            if (this.ordinaryLuggage[i].getWeight() <= 32 && this.ordinaryLuggage[i].getWeight() > 28)
                                result += 520;
                            this.freeCheckedBaggage.number--;
                        } else {//超额第一件/第二件
                            if (this.freeCheckedBaggage.number == 0) {
                                result += 830 + getMoneyInArea5(100, this.ordinaryLuggage[i].getWeight());
                                this.freeCheckedBaggage.number--;
                            } else if (this.freeCheckedBaggage.number == -1) {
                                result += 1100 + getMoneyInArea5(100, this.ordinaryLuggage[i].getWeight());
                                this.freeCheckedBaggage.number--;
                            } else {//超额第三件
                                result += 1590 + getMoneyInArea5(100, this.ordinaryLuggage[i].getWeight());
                                ;
                                this.freeCheckedBaggage.number--;
                            }
                        }
                    } else {//不计入免费行李额度
                        result += getSpecialCost(this.specialLuggage[i].getLuggageType(), this.specialLuggage[i].getWeight());
                    }
                }
            }
        }
        return result;
    }




    //区域1免费托运行李超重或者超尺寸费用
    public int getMoneyInArea1(int baggageSize,int baggageWeight){
        int result=0;
        if(baggageWeight>23&&baggageWeight<=28&&baggageSize<=158){
            result=380;
        }
        else if(baggageWeight>28&&baggageWeight<=32&&baggageSize<=158){
            result=980;
        }
        else if(baggageWeight>2&&baggageWeight<=23&&baggageSize<=203&&baggageSize>158){
            result=980;
        }
        else if(baggageWeight>23&&baggageWeight<=32&&baggageSize<=203&&baggageSize>158){
            result=1400;
        }
        return result;
    }

    //区域2免费托运行李超重或者超尺寸费用
    public int getMoneyInArea2(int baggageSize,int baggageWeight){
        int result=0;
        if(baggageWeight>23&&baggageWeight<=28&&baggageSize<=158){
            result=280;
        }
        else if(baggageWeight>28&&baggageWeight<=32&&baggageSize<=158){
            result=690;
        }
        else if(baggageWeight>2&&baggageWeight<=23&&baggageSize<=203&&baggageSize>158){
            result=690;
        }
        else if(baggageWeight>23&&baggageWeight<=32&&baggageSize<=203&&baggageSize>158){
            result=1100;
        }
        return result;
    }

    //区域3免费托运行李超重或者超尺寸费用
    public int getMoneyInArea3(int baggageSize,int baggageWeight){
        int result=0;
        if(baggageSize>=158&&baggageSize<=203||baggageWeight>23&&baggageWeight<=32)result=520;
        if(baggageSize>=158&&baggageSize<=203&&baggageWeight>23&&baggageWeight<=32)result=520;
        return result;
    }

    //区域4免费托运行李超重或者超尺寸费用
    public int getMoneyInArea4(int baggageSize,int baggageWeight){
        int result=0;
        if(baggageWeight>23&&baggageWeight<=28&&baggageSize<=158){
            result=690;
        }
        else if(baggageWeight>28&&baggageWeight<=32&&baggageSize<=158){
            result=1040;
        }
        else if(baggageWeight>2&&baggageWeight<=23&&baggageSize<=203&&baggageSize>158){
            result=1040;
        }
        else if(baggageWeight>23&&baggageWeight<=32&&baggageSize<=203&&baggageSize>158){
            result=2050;
        }
        return result;
    }

    //区域5免费托运行李超重或者超尺寸费用
    public int getMoneyInArea5(int baggageSize,int baggageWeight){
        int result=0;
        if(baggageWeight>23&&baggageWeight<=28&&baggageSize<=158){
            result=210;
        }
        else if(baggageWeight>28&&baggageWeight<=32&&baggageSize<=158){
            result=520;
        }
        else if(baggageWeight>2&&baggageWeight<=23&&baggageSize<=203&&baggageSize>158){
            result=520;
        }
        else if(baggageWeight>23&&baggageWeight<=32&&baggageSize<=203&&baggageSize>158){
            result=830;
        }
        return result;
    }


    //计算不计入免费托运额度的特殊行李
    public int getSpecialCost(String specialType,int baggageWeight){
        int result=0;
        switch (specialType){
            case "类别3":
                if(baggageWeight>=2&&baggageWeight<=23)result+=2600;
                if(baggageWeight>23&&baggageWeight<=32)result+=3900;
                if(baggageWeight>32&&baggageWeight<=45)result+=5200;
                break;

            case "类别4":
                if(baggageWeight>=2&&baggageWeight<=23)result+=1300;
                if(baggageWeight>23&&baggageWeight<=32)result+=2600;
                if(baggageWeight>32&&baggageWeight<=45)result+=3900;
                break;

            case "类别6":
                if(baggageWeight>=2&&baggageWeight<=23)result+=490;
                if(baggageWeight>23&&baggageWeight<=32)result+=3900;
                break;

            case "类别7":
                if(baggageWeight>=2&&baggageWeight<=23)result+=1300;
                if(baggageWeight>23&&baggageWeight<=32)result+=2600;
                break;

            case "类别8":
                if(baggageWeight>=2&&baggageWeight<=5)result+=1300;
                break;

            case "类别9":
                if(baggageWeight>=2&&baggageWeight<=8)result+=3900;
                if(baggageWeight>8&&baggageWeight<=23)result+=5200;
                if(baggageWeight>23&&baggageWeight<=32)result+=7800;
                break;
            default:break;
        }
        return result;
    }

}
