package com.example.springboot;

public class InternationalPassengers {
    String shippingSpace;//旅客舱位
    String region1; //经济舱免费托运行李区域
    String routeRegion; //航线区域

    public String getShippingSpace(){
        return shippingSpace;
    }

    public void setShippingSpace(String shippingSpace){
        this.shippingSpace = shippingSpace;
    }

    public String getRegion1(){
        return region1;
    }

    public void setRegion1(String region1){
        this.region1 = region1;
    }

    public String getRouteRegion(){
        return routeRegion;
    }

    public void setRouteRegion(String routeRegion){
        this.routeRegion = routeRegion;
    }
}
