package com.example.springboot;

import com.example.springboot.someDataStruct.freeCheckedBaggage;
import com.example.springboot.someDataStruct.internationalExcessCost;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.kerberos.KerberosTicket;
import java.util.ArrayList;

@Controller
public class LController {
    public OrdinaryLuggage[] OrdinaryLuggage;
    public InternationalPassengers InternationalPassengers;
    public SpecialLuggage[] SpecialLuggage;
    public DomesticPassenger DomesticPassenger;
    public freeCheckedBaggage freeBaggage=new freeCheckedBaggage();
    public int maxFreeCost=0;


    @CrossOrigin
    @PostMapping
    @ResponseBody
    @RequestMapping("/test")
    public Result getOrdinaryLuggage(@RequestBody OrdinaryLuggage[] OrdinaryLuggage){
        this.OrdinaryLuggage= OrdinaryLuggage;
        for(int i=0;i<this.OrdinaryLuggage.length;i++) {
            System.out.println(OrdinaryLuggage[i].len);
            System.out.println(OrdinaryLuggage[i].width);
            System.out.println(OrdinaryLuggage[i].height);
            System.out.println(OrdinaryLuggage[i].weight);
            System.out.println(OrdinaryLuggage[i].airLine);
        }

        String str="普通行李共有"+this.OrdinaryLuggage.length+"件，添加成功！";
        Result result=new Result(200,str);
        if(this.OrdinaryLuggage==null){
            result.setMessage("添加失败");
        }
        return result;
    }

    @CrossOrigin
    @PostMapping
    @ResponseBody
    @RequestMapping("/internationalPassenger")
    public Result internationalPassenger(@RequestBody InternationalPassengers InternationalPassengers){
        this.InternationalPassengers=InternationalPassengers;
        System.out.println(InternationalPassengers.shippingSpace);
        System.out.println(InternationalPassengers.region1);
        System.out.println(InternationalPassengers.routeRegion);

        Result result=new Result(200,"添加成功");
        if(this.DomesticPassenger==null){
            result.setCode(203);
            result.setMessage("旅客信息为空，添加失败");
        }
        return result;

    }

    @CrossOrigin
    @PostMapping
    @ResponseBody
    @RequestMapping("/DomesticPassenger")
    public Result domesticPassenger(@RequestBody DomesticPassenger DomesticPassenger){
        this.DomesticPassenger =DomesticPassenger;
        System.out.println(this.DomesticPassenger.ticketType);
        System.out.println(this.DomesticPassenger.shippingSpace);
        System.out.println(this.DomesticPassenger.member);
        System.out.println(this.DomesticPassenger.price);


        Result result=new Result(200,"添加成功");
        if(this.DomesticPassenger==null){
            result.setMessage("旅客信息为空，添加失败");
        }
        return result;
    }

    @CrossOrigin
    @PostMapping
    @ResponseBody
    @RequestMapping("/specialLuggage1")
    public Result getSpecialLuggage(@RequestBody SpecialLuggage[] specialLuggage){
        this.SpecialLuggage =specialLuggage;

        for(int i = 0; i<this.SpecialLuggage.length; i++) {
            System.out.println(SpecialLuggage[i].luggageType);
            System.out.println(SpecialLuggage[i].weight);
        }
        String str="特殊行李共有"+this.SpecialLuggage.length+"件，添加成功！";
        Result result=new Result(200,str);
        if(this.SpecialLuggage==null){
            result.setCode(203);
            result.setMessage("添加失败");
        }

        //if(OrdinaryLuggage==null)System.out.println("普通行李为空！！");
        return result;
        //return new Result(200);specialTypesLuggage
    }


    @CrossOrigin
    @PostMapping
    @ResponseBody
    @RequestMapping("/get")
    public Result getResult(){
        Result result=new Result(200,"添加成功！");
        if(this.InternationalPassengers==null&&this.DomesticPassenger==null){
            result.setMessage("请先添加一个乘客信息！");
        }
        else if(this.InternationalPassengers!=null&&this.DomesticPassenger!=null){
            result.setMessage("只能添加一个乘客信息！");
        }
        else if(this.OrdinaryLuggage==null&&this.SpecialLuggage==null){
            result.setMessage("请添加行李信息！");
        }
        else{
            int ord=0;
            int spe=0;
            if(this.OrdinaryLuggage!=null)ord=this.OrdinaryLuggage.length;
            if(this.SpecialLuggage!=null)spe=this.SpecialLuggage.length;
            String str="共有"+ord+"件普通行李；"+spe+"件特殊行李；总计"+String.format("%.2f",this.totalCost())+"元";
            result.setMessage(str);
        }
        this.clearAll();
        return result;
    }


    public void clearAll(){
        this.OrdinaryLuggage=null;
        this.SpecialLuggage=null;
        this.InternationalPassengers=null;
        this.DomesticPassenger=null;
        this.maxFreeCost=0;
    }


    public void setMaxFreeCost(){
        if(this.DomesticPassenger.ticketType.equals("1")){
            if(this.DomesticPassenger.shippingSpace.equals("头等舱"))this.maxFreeCost+=40;
            else if(this.DomesticPassenger.shippingSpace.equals("公务舱"))this.maxFreeCost+=30;
            else this.maxFreeCost+=20;
        }
        else this.maxFreeCost+=10;


        if (this.DomesticPassenger.member.equals("选项2")){
            this.maxFreeCost+=30;
        }
        else if (this.DomesticPassenger.member.equals("选项3")||this.DomesticPassenger.member.equals("选项4")){
            this.maxFreeCost+=20;
        }
    }

    public double totalCost(){
        double totalCost=0;
        internationalExcessCost temp = new internationalExcessCost();
        if(this.InternationalPassengers!=null){//国际航线旅客
            switch (this.InternationalPassengers.shippingSpace){
                case "选项1":
                    this.freeBaggage.number=2;
                    this.freeBaggage.eachWeight=32;
                    break;
                case "选项2":
                    this.freeBaggage.number=2;
                    this.freeBaggage.eachWeight=23;
                    break;

                case "选项3":
                    if(this.InternationalPassengers.region1.equals("1")){
                        this.freeBaggage.number=1;
                    }
                    else {
                        this.freeBaggage.number=2;
                    }
                    this.freeBaggage.eachWeight=23;
                    break;
                default:break;
            }

            temp.setValue(this.OrdinaryLuggage,this.SpecialLuggage,this.freeBaggage);

            switch (this.InternationalPassengers.routeRegion){
                case "选项1":
                    totalCost+=temp.area1(this.freeBaggage.eachWeight);
                    break;
                case "选项2":
                    totalCost+=temp.area2(this.freeBaggage.eachWeight);
                    break;
                case "选项3":
                    totalCost+=temp.area3(this.freeBaggage.eachWeight);
                    break;
                case "选项4":
                    totalCost+=temp.area4(this.freeBaggage.eachWeight);
                    break;
                case "选项5":
                    totalCost+=temp.area5(this.freeBaggage.eachWeight);
                    break;
            }
        }

        if(this.DomesticPassenger!=null){//国内航线旅客
            this.setMaxFreeCost();
            switch (this.DomesticPassenger.shippingSpace){
                case "头等舱": case "公务舱":
                    if(this.OrdinaryLuggage!=null) {
                        for (int i = 0; i < this.OrdinaryLuggage.length; i++) {
                            if (this.maxFreeCost == 0) {
                                totalCost += this.OrdinaryLuggage[i].getWeight() * 0.015 * this.DomesticPassenger.price;
                            } else if (this.OrdinaryLuggage[i].getWeight() <= this.maxFreeCost && this.maxFreeCost != 0) {
                                this.maxFreeCost -= this.OrdinaryLuggage[i].getWeight();
                            } else if (this.OrdinaryLuggage[i].getWeight() > this.maxFreeCost && this.maxFreeCost != 0) {
                                totalCost += (this.OrdinaryLuggage[i].getWeight() - this.maxFreeCost) * 0.015 * this.DomesticPassenger.price;
                                this.maxFreeCost = 0;
                            }
                        }
                    }

                        if (this.SpecialLuggage != null) {
                            for (int i = 0; i < this.SpecialLuggage.length; i++) {
                                if (this.SpecialLuggage[i].getLuggageType().equals("类别2") || this.SpecialLuggage[i].getLuggageType().equals("类别5")) {//计入免费行李额度的类别
                                    if (this.maxFreeCost == 0) {
                                        totalCost += this.SpecialLuggage[i].getWeight() * 0.015 * this.DomesticPassenger.price;
                                    } else if (this.SpecialLuggage[i].getWeight() <= this.maxFreeCost && this.maxFreeCost != 0) {
                                        this.maxFreeCost -= this.SpecialLuggage[i].getWeight();
                                    } else if (this.SpecialLuggage[i].getWeight() > this.maxFreeCost && this.maxFreeCost != 0) {
                                        totalCost += (this.SpecialLuggage[i].getWeight() - this.maxFreeCost) * 0.015 * this.DomesticPassenger.price;
                                        this.maxFreeCost = 0;
                                    }
                                }
                                else totalCost+=temp.getSpecialCost(this.SpecialLuggage[i].getLuggageType(),this.SpecialLuggage[i].getWeight());
                            }
                        }
                        break;
                case "经济舱":
                    if(this.OrdinaryLuggage!=null){
                        for(int i=0;i<this.OrdinaryLuggage.length;i++){
                            if(this.maxFreeCost==0){
                                if(this.OrdinaryLuggage[i].getWeight()<=23){//不超重量
                                    totalCost+=this.OrdinaryLuggage[i].getWeight()*0.015*this.DomesticPassenger.price;
                                }
                                else{//超重量
                                    totalCost+=this.OrdinaryLuggage[i].getWeight()*0.015*this.DomesticPassenger.price+(this.OrdinaryLuggage[i].getWeight()-23)*0.015*this.DomesticPassenger.price;
                                }

                            }
                            else if(this.OrdinaryLuggage[i].getWeight()<=this.maxFreeCost&&this.maxFreeCost!=0){
                                if(this.OrdinaryLuggage[i].getWeight()>23)totalCost+=(this.OrdinaryLuggage[i].getWeight()-23)*0.015*this.DomesticPassenger.price;
                                this.maxFreeCost-=this.OrdinaryLuggage[i].getWeight();
                            }
                            else if(this.OrdinaryLuggage[i].getWeight()>this.maxFreeCost&&this.maxFreeCost!=0){
                                if(this.OrdinaryLuggage[i].getWeight()<=23){//不超重量
                                    totalCost += (this.OrdinaryLuggage[i].getWeight() - this.maxFreeCost) * 0.015 * this.DomesticPassenger.price;
                                    this.maxFreeCost = 0;
                                }
                                else{//超重量
                                    totalCost += (this.OrdinaryLuggage[i].getWeight() - this.maxFreeCost) * 0.015 * this.DomesticPassenger.price+(this.OrdinaryLuggage[i].getWeight()-23)*0.015*this.DomesticPassenger.price;;
                                    this.maxFreeCost = 0;
                                }
                            }
                        }
                    }
                    if (this.SpecialLuggage != null) {
                        for (int i = 0; i < this.SpecialLuggage.length; i++) {//特殊行李重量限制为32KG
                            if (this.SpecialLuggage[i].getLuggageType().equals("类别2") || this.SpecialLuggage[i].getLuggageType().equals("类别5") ) {//计入免费行李额度的类别
                                if (this.maxFreeCost == 0) {
                                    totalCost += this.SpecialLuggage[i].getWeight() * 0.015 * this.DomesticPassenger.price;
                                } else if (this.SpecialLuggage[i].getWeight() <= this.maxFreeCost && this.maxFreeCost != 0) {
                                    this.maxFreeCost -= this.SpecialLuggage[i].getWeight();
                                } else if (this.SpecialLuggage[i].getWeight() > this.maxFreeCost && this.maxFreeCost != 0) {
                                    totalCost += (this.SpecialLuggage[i].getWeight() - this.maxFreeCost) * 0.015 * this.DomesticPassenger.price;
                                    this.maxFreeCost = 0;
                                }
                            }
                            else totalCost+=temp.getSpecialCost(this.SpecialLuggage[i].getLuggageType(),this.SpecialLuggage[i].getWeight());
                        }
                    }
                    break;
                default:break;
            }
        }
        return totalCost;
    }

}

