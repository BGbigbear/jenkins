package com.example.springboot.mytest;
import com.example.springboot.*;
import com.example.springboot.someDataStruct.freeCheckedBaggage;
import com.example.springboot.someDataStruct.internationalExcessCost;
import com.fasterxml.jackson.databind.ObjectReader;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.*;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;




public class unitTest {

    public LController test=new LController();
    public InternationalPassengers inter=new InternationalPassengers();
    public DomesticPassenger dom=new DomesticPassenger();
    public OrdinaryLuggage[] ord=new OrdinaryLuggage[1];
    public SpecialLuggage[] spe=new SpecialLuggage[1];



    //    对一些国内旅客信息部分进行测试
    @Test(testName = "国内航线黑盒测试")
    public void blackTest1() throws IOException {
        System.out.println("国内航线黑盒测试开始");
        FileInputStream f=new FileInputStream("src/main/resources/static/黑盒测试用例1.xlsx");
        XSSFWorkbook wb=new XSSFWorkbook(f);
        XSSFSheet sheet=wb.getSheetAt(0);
        SpecialLuggage Spe=new SpecialLuggage();
        OrdinaryLuggage Ord=new OrdinaryLuggage();
        for(int j=1;j<sheet.getLastRowNum();j++) {
            XSSFRow row = sheet.getRow(j);
            for(int i=0;i<row.getLastCellNum();i++){
                if(i==0)System.out.print('[');
                System.out.print(row.getCell(i));
                if(i<row.getLastCellNum()-1)System.out.print(',');
            }
            System.out.println(']');
            XSSFCell tes=row.getCell(0);
            tes.setCellType(CellType.STRING);
            dom.setTicketType(tes.toString());
            tes=row.getCell(3);
            tes.setCellType(CellType.STRING);
            dom.setPrice(Integer.valueOf(tes.toString()));

            dom.setShippingSpace(row.getCell(2).toString());
            dom.setMember(row.getCell(1).toString());

            tes=row.getCell(4);
            tes.setCellType(CellType.STRING);
            Ord.setLen(Integer.valueOf(tes.toString()));

            tes=row.getCell(5);
            tes.setCellType(CellType.STRING);
            Ord.setWidth(Integer.valueOf(tes.toString()));

            tes=row.getCell(6);
            tes.setCellType(CellType.STRING);
            Ord.setHeight(Integer.valueOf(tes.toString()));

            tes=row.getCell(7);
            tes.setCellType(CellType.STRING);
            Ord.setWeight(Integer.valueOf(tes.toString()));

            Ord.setAirLine(row.getCell(8).toString());
            Spe.setLuggageType(row.getCell(9).toString());

            tes=row.getCell(10);
            tes.setCellType(CellType.STRING);
            Spe.setWeight(Integer.valueOf(tes.toString()));

            this.ord[0]=Ord;
            this.spe[0]=Spe;
            this.test.DomesticPassenger=dom;
            this.test.OrdinaryLuggage=ord;
            this.test.SpecialLuggage=spe;
            double result=Double.valueOf(row.getCell(11).toString());
            Assert.assertEquals(result,this.test.totalCost());
            test.clearAll();
        }
        System.out.println("国内航线黑盒测试结束");
    }


//    对一些国际旅客信息部分进行测试

    @Test(testName = "国际航线黑盒测试")
    public void blackTest2() throws IOException {
        System.out.println("国际航线黑盒测试开始");
        FileInputStream f=new FileInputStream("src/main/resources/static/黑盒测试用例1.xlsx");
        XSSFWorkbook wb=new XSSFWorkbook(f);
        XSSFSheet sheet=wb.getSheetAt(1);
        SpecialLuggage Spe=new SpecialLuggage();
        OrdinaryLuggage Ord=new OrdinaryLuggage();
        for(int i=1;i<sheet.getLastRowNum();i++){
            XSSFRow row=sheet.getRow(i);
            for(int j=0;j<row.getLastCellNum();j++){
                if(j==0)System.out.print('[');
                System.out.print(row.getCell(j));
                if(j<row.getLastCellNum()-1)System.out.print(',');
            }
            System.out.println(']');
            this.inter.setShippingSpace(row.getCell(0).toString());

            XSSFCell tes=row.getCell(1);
            tes.setCellType(CellType.STRING);
            this.inter.setRegion1(tes.toString());

            this.inter.setRouteRegion(row.getCell(2).toString());

            tes=row.getCell(3);
            tes.setCellType(CellType.STRING);
            Ord.setLen(Integer.valueOf(tes.toString()));

            tes=row.getCell(4);
            tes.setCellType(CellType.STRING);
            Ord.setWidth(Integer.valueOf(tes.toString()));

            tes=row.getCell(5);
            tes.setCellType(CellType.STRING);
            Ord.setHeight(Integer.valueOf(tes.toString()));

            tes=row.getCell(6);
            tes.setCellType(CellType.STRING);
            Ord.setWeight(Integer.valueOf(tes.toString()));

            Ord.setAirLine(row.getCell(7).toString());

            Spe.setLuggageType(row.getCell(8).toString());

            tes=row.getCell(9);
            tes.setCellType(CellType.STRING);
            Spe.setWeight(Integer.valueOf(tes.toString()));

            this.ord[0]=Ord;
            this.spe[0]=Spe;
            this.test.InternationalPassengers=inter;
            this.test.OrdinaryLuggage=ord;
            this.test.SpecialLuggage=spe;

            double result=Double.valueOf(row.getCell(10).toString());
            Assert.assertEquals(result,this.test.totalCost());
            test.clearAll();
        }
        System.out.println("国际航线黑盒测试结束");
    }

    @Test(testName = "计算国际航线费用area1-area5测试")
    public void whiteTest1(){
        //函数area1测试
        System.out.println("计算国际航线费用area1-area5测试开始");
        System.out.println("area1测试开始");
        internationalExcessCost whiteTest=new internationalExcessCost();
        freeCheckedBaggage free=new freeCheckedBaggage();
        free.eachWeight=32;
        free.number=2;
        OrdinaryLuggage[] ordinaryLuggage=new OrdinaryLuggage[5];
        SpecialLuggage[] specialLuggage=new SpecialLuggage[1];
        OrdinaryLuggage temp=new OrdinaryLuggage();
        temp.setLen(50);
        temp.setWidth(40);
        temp.setHeight(34);
        temp.setWeight(24);
        temp.setAirLine("类别2");
        ordinaryLuggage[0]=temp;
        for(int i=1;i<5;i++){
            OrdinaryLuggage temp1=new OrdinaryLuggage();
            temp1.setLen(80);
            temp1.setWidth(50);
            temp1.setHeight(34);
            temp1.setWeight(24);
            temp1.setAirLine("类别2");
            ordinaryLuggage[i]=temp1;
        }
        SpecialLuggage spe=new SpecialLuggage();
        spe.setWeight(4);
        spe.setLuggageType("类别8");
        specialLuggage[0]=spe;
        whiteTest.specialLuggage=specialLuggage;
        whiteTest.ordinaryLuggage=ordinaryLuggage;
        whiteTest.freeCheckedBaggage=free;
        Assert.assertEquals(11620,whiteTest.area1(32));


        free.eachWeight=32;
        free.number=2;
        OrdinaryLuggage[] ordinaryLuggage1=new OrdinaryLuggage[3];
        temp.setLen(80);
        temp.setWidth(50);
        temp.setHeight(34);
        temp.setWeight(23);
        temp.setAirLine("类别2");
        ordinaryLuggage1[0]=temp;
        for(int i=1;i<3;i++){
            OrdinaryLuggage temp1=new OrdinaryLuggage();
            temp1.setLen(50);
            temp1.setWidth(50);
            temp1.setHeight(34);
            temp1.setWeight(23);
            temp1.setAirLine("类别2");
            ordinaryLuggage1[i]=temp1;
        }
        whiteTest.specialLuggage=null;
        whiteTest.ordinaryLuggage=ordinaryLuggage1;
        whiteTest.freeCheckedBaggage=free;
        Assert.assertEquals(2380,whiteTest.area1(32));




        //计算区域2
        System.out.println("area2测试开始");
        free.eachWeight=32;
        free.number=2;
        temp.setLen(50);
        temp.setWidth(40);
        temp.setHeight(34);
        temp.setWeight(24);
        temp.setAirLine("类别2");
        ordinaryLuggage[0]=temp;
        for(int i=1;i<5;i++){
            OrdinaryLuggage temp1=new OrdinaryLuggage();
            temp1.setLen(80);
            temp1.setWidth(50);
            temp1.setHeight(34);
            temp1.setWeight(24);
            temp1.setAirLine("类别2");
            ordinaryLuggage[i]=temp1;
        }
        spe.setWeight(4);
        spe.setLuggageType("类别8");
        specialLuggage[0]=spe;
        whiteTest.specialLuggage=specialLuggage;
        whiteTest.ordinaryLuggage=ordinaryLuggage;
        whiteTest.freeCheckedBaggage=free;
        Assert.assertEquals(7850,whiteTest.area2(32));


        free.eachWeight=32;
        free.number=2;
        temp.setLen(80);
        temp.setWidth(50);
        temp.setHeight(34);
        temp.setWeight(23);
        temp.setAirLine("类别2");
        ordinaryLuggage1[0]=temp;
        for(int i=1;i<3;i++){
            OrdinaryLuggage temp1=new OrdinaryLuggage();
            temp1.setLen(50);
            temp1.setWidth(50);
            temp1.setHeight(34);
            temp1.setWeight(23);
            temp1.setAirLine("类别2");
            ordinaryLuggage1[i]=temp1;
        }
        whiteTest.specialLuggage=null;
        whiteTest.ordinaryLuggage=ordinaryLuggage1;
        whiteTest.freeCheckedBaggage=free;
        Assert.assertEquals(1790,whiteTest.area2(32));


        //计算区域3
        System.out.println("area3测试开始");
        free.eachWeight=32;
        free.number=2;
        temp.setLen(50);
        temp.setWidth(40);
        temp.setHeight(34);
        temp.setWeight(24);
        temp.setAirLine("类别2");
        ordinaryLuggage[0]=temp;
        for(int i=1;i<5;i++){
            OrdinaryLuggage temp1=new OrdinaryLuggage();
            temp1.setLen(80);
            temp1.setWidth(50);
            temp1.setHeight(34);
            temp1.setWeight(24);
            temp1.setAirLine("类别2");
            ordinaryLuggage[i]=temp1;
        }
        spe.setWeight(4);
        spe.setLuggageType("类别8");
        specialLuggage[0]=spe;
        whiteTest.specialLuggage=specialLuggage;
        whiteTest.ordinaryLuggage=ordinaryLuggage;
        whiteTest.freeCheckedBaggage=free;
        Assert.assertEquals(7310,whiteTest.area3(32));


        free.eachWeight=32;
        free.number=2;
        temp.setLen(80);
        temp.setWidth(50);
        temp.setHeight(34);
        temp.setWeight(23);
        temp.setAirLine("类别2");
        ordinaryLuggage1[0]=temp;
        for(int i=1;i<3;i++){
            OrdinaryLuggage temp1=new OrdinaryLuggage();
            temp1.setLen(50);
            temp1.setWidth(50);
            temp1.setHeight(34);
            temp1.setWeight(23);
            temp1.setAirLine("类别2");
            ordinaryLuggage1[i]=temp1;
        }
        whiteTest.specialLuggage=null;
        whiteTest.ordinaryLuggage=ordinaryLuggage1;
        whiteTest.freeCheckedBaggage=free;
        Assert.assertEquals(1690,whiteTest.area3(32));


        //计算区域4
        System.out.println("area4测试开始");
        free.eachWeight=32;
        free.number=2;
        temp.setLen(50);
        temp.setWidth(40);
        temp.setHeight(34);
        temp.setWeight(24);
        temp.setAirLine("类别2");
        ordinaryLuggage[0]=temp;
        for(int i=1;i<5;i++){
            OrdinaryLuggage temp1=new OrdinaryLuggage();
            temp1.setLen(80);
            temp1.setWidth(50);
            temp1.setHeight(34);
            temp1.setWeight(24);
            temp1.setAirLine("类别2");
            ordinaryLuggage[i]=temp1;
        }
        spe.setWeight(4);
        spe.setLuggageType("类别8");
        specialLuggage[0]=spe;
        whiteTest.specialLuggage=specialLuggage;
        whiteTest.ordinaryLuggage=ordinaryLuggage;
        whiteTest.freeCheckedBaggage=free;
        Assert.assertEquals(9810,whiteTest.area4(32));


        free.eachWeight=32;
        free.number=2;
        temp.setLen(80);
        temp.setWidth(50);
        temp.setHeight(34);
        temp.setWeight(23);
        temp.setAirLine("类别2");
        ordinaryLuggage1[0]=temp;
        for(int i=1;i<3;i++){
            OrdinaryLuggage temp1=new OrdinaryLuggage();
            temp1.setLen(50);
            temp1.setWidth(50);
            temp1.setHeight(34);
            temp1.setWeight(23);
            temp1.setAirLine("类别2");
            ordinaryLuggage1[i]=temp1;
        }
        whiteTest.specialLuggage=null;
        whiteTest.ordinaryLuggage=ordinaryLuggage1;
        whiteTest.freeCheckedBaggage=free;
        Assert.assertEquals(2420,whiteTest.area4(32));


        //计算区域5
        System.out.println("area5测试开始");
        free.eachWeight=32;
        free.number=2;
        temp.setLen(50);
        temp.setWidth(40);
        temp.setHeight(34);
        temp.setWeight(24);
        temp.setAirLine("类别2");
        ordinaryLuggage[0]=temp;
        for(int i=1;i<5;i++){
            OrdinaryLuggage temp1=new OrdinaryLuggage();
            temp1.setLen(80);
            temp1.setWidth(50);
            temp1.setHeight(34);
            temp1.setWeight(24);
            temp1.setAirLine("类别2");
            ordinaryLuggage[i]=temp1;
        }
        spe.setWeight(4);
        spe.setLuggageType("类别8");
        specialLuggage[0]=spe;
        whiteTest.specialLuggage=specialLuggage;
        whiteTest.ordinaryLuggage=ordinaryLuggage;
        whiteTest.freeCheckedBaggage=free;
        Assert.assertEquals(6900,whiteTest.area5(32));


        free.eachWeight=32;
        free.number=2;
        temp.setLen(80);
        temp.setWidth(50);
        temp.setHeight(34);
        temp.setWeight(23);
        temp.setAirLine("类别2");
        ordinaryLuggage1[0]=temp;
        for(int i=1;i<3;i++){
            OrdinaryLuggage temp1=new OrdinaryLuggage();
            temp1.setLen(50);
            temp1.setWidth(50);
            temp1.setHeight(34);
            temp1.setWeight(23);
            temp1.setAirLine("类别2");
            ordinaryLuggage1[i]=temp1;
        }
        whiteTest.specialLuggage=null;
        whiteTest.ordinaryLuggage=ordinaryLuggage1;
        whiteTest.freeCheckedBaggage=free;
        Assert.assertEquals(1350,whiteTest.area5(32));
        System.out.println("计算国际航线费用area1-area5测试结束");
    }

    @Test(testName = "totalCost国际航线测试")
    public void whiteTest3(){
        //测试用例编号 1、2、3、4、5
        System.out.println("totalCost国际航线测试开始");
        DomesticPassenger dom=new DomesticPassenger();
        InternationalPassengers inter=new InternationalPassengers();
        inter.setShippingSpace("选项1");
        inter.setRegion1("1");
        inter.setRouteRegion("选项1");
        OrdinaryLuggage[] ord1=new OrdinaryLuggage[1];
        SpecialLuggage[] spe1=new SpecialLuggage[1];
        ord1[0]=new OrdinaryLuggage();
        ord1[0].setLen(80);
        ord1[0].setWidth(50);
        ord1[0].setHeight(32);
        ord1[0].setWeight(32);

        spe1[0]=new SpecialLuggage();
        spe1[0].setWeight(25);
        spe1[0].setLuggageType("类别9");

        LController test=new LController();
        test.OrdinaryLuggage=ord1;
        test.SpecialLuggage=spe1;
        test.InternationalPassengers=inter;
        Assert.assertEquals(8780,test.totalCost());
        test.clearAll();

        inter.setRouteRegion("选项2");
        test.OrdinaryLuggage=ord1;
        test.SpecialLuggage=spe1;
        test.InternationalPassengers=inter;
        Assert.assertEquals(8490,test.totalCost());
        test.clearAll();

        inter.setRouteRegion("选项3");
        test.OrdinaryLuggage=ord1;
        test.SpecialLuggage=spe1;
        test.InternationalPassengers=inter;
        Assert.assertEquals(8320,test.totalCost());
        test.clearAll();

        inter.setRouteRegion("选项4");
        test.OrdinaryLuggage=ord1;
        test.SpecialLuggage=spe1;
        test.InternationalPassengers=inter;
        Assert.assertEquals(8840,test.totalCost());
        test.clearAll();

        inter.setRouteRegion("选项5");
        test.OrdinaryLuggage=ord1;
        test.SpecialLuggage=spe1;
        test.InternationalPassengers=inter;
        Assert.assertEquals(8320,test.totalCost());
        test.clearAll();
        System.out.println("totalCost国际航线测试结束");
    }

    @Test(testName = "totalCost国内航线测试")
    public void whiteTest4(){
        //测试用例编号 6、8、9
        System.out.println("totalCost国内航线测试开始");
        OrdinaryLuggage[] ord=new OrdinaryLuggage[3];
        SpecialLuggage[] spe=new SpecialLuggage[4];
        DomesticPassenger dom=new DomesticPassenger();
        LController test=new LController();
        dom.setMember("选项1");
        dom.setShippingSpace("头等舱");
        dom.setPrice(1000);
        dom.setTicketType("1");

        for(int i=0;i<3;i++){
            ord[i]=new OrdinaryLuggage();
            ord[i].setLen(50);
            ord[i].setWidth(40);
            ord[i].setHeight(34);
            ord[i].setAirLine("类别1");
        }
        ord[0].setWeight(32);
        ord[1].setWeight(28);
        ord[2].setWeight(20);
        test.DomesticPassenger=dom;
        test.OrdinaryLuggage=ord;
        Assert.assertEquals(600,test.totalCost());

        test.maxFreeCost=0;
        dom.setShippingSpace("经济舱");
        test.OrdinaryLuggage[0].setWeight(17);
        test.OrdinaryLuggage[1].setWeight(13);
        test.OrdinaryLuggage[2].setWeight(20);
        Assert.assertEquals(450,test.totalCost());

        test.maxFreeCost=0;
        dom.setMember("选项4");
        test.OrdinaryLuggage[0].setWeight(32);
        test.OrdinaryLuggage[1].setWeight(28);
        test.OrdinaryLuggage[2].setWeight(30);
        Assert.assertEquals(1065,test.totalCost());
        test.clearAll();

        //测试用例编号 7、10
        dom.setMember("选项1");
        test.DomesticPassenger=dom;
       test.DomesticPassenger.setMember("选项1");
       test.DomesticPassenger.setShippingSpace("头等舱");

       test.SpecialLuggage=spe;
        test.SpecialLuggage[0]=new SpecialLuggage();
        test.SpecialLuggage[0].setLuggageType("类别2");
        test.SpecialLuggage[0].setWeight(32);

        test.SpecialLuggage[1]=new SpecialLuggage();
        test.SpecialLuggage[1].setLuggageType("类别5");
        test.SpecialLuggage[1].setWeight(28);

        test.SpecialLuggage[2]=new SpecialLuggage();
        test.SpecialLuggage[2].setLuggageType("类别2");
        test.SpecialLuggage[2].setWeight(20);

        test.SpecialLuggage[3]=new SpecialLuggage();
        test.SpecialLuggage[3].setLuggageType("类别8");
        test.SpecialLuggage[3].setWeight(4);
        Assert.assertEquals(1900,test.totalCost());

        test.maxFreeCost=0;
        test.DomesticPassenger.setMember("选项4");
        test.DomesticPassenger.setShippingSpace("经济舱");
        Assert.assertEquals(1900,test.totalCost());
        System.out.println("totalCost国内航线测试结束");

    }

    @Test(testName = "函数getMoneyInArea1-getMoneyInArea1测试")
    public void whiteTest5() throws IOException {
        System.out.println("函数getMoneyInArea1-getMoneyInArea1测试开始");
        FileInputStream f=new FileInputStream("src/main/resources/static/黑盒测试用例1.xlsx");
        XSSFWorkbook wb=new XSSFWorkbook(f);
        XSSFSheet sheet=wb.getSheetAt(2);
        internationalExcessCost test1=new internationalExcessCost();
        int baggageSize=0;
        int baggageWeight=0;
        int exception=0;
        for(int i=0;i<25;i++){
            XSSFRow row=sheet.getRow(i%5+1);
            for(int j=0;j<row.getLastCellNum();j++){
                if(j==0)System.out.print('[');
                System.out.print(row.getCell(j));
                if(j<row.getLastCellNum()-1)System.out.print(',');
            }
            System.out.println(']');
            XSSFRow row1=sheet.getRow(i+1);
            XSSFCell tes=row.getCell(0);
            tes.setCellType(CellType.STRING);
            baggageSize=Integer.valueOf(tes.toString());
            tes=row.getCell(1);
            tes.setCellType(CellType.STRING);
            baggageWeight=Integer.valueOf(tes.toString());
            tes=row1.getCell(2);
            tes.setCellType(CellType.STRING);
            exception=Integer.valueOf(tes.toString());

            switch (i/5){
                case 0:
                    Assert.assertEquals(exception,test1.getMoneyInArea1(baggageSize,baggageWeight));
                    break;
                case 1:
                    Assert.assertEquals(exception,test1.getMoneyInArea2(baggageSize,baggageWeight));
                    break;
                case 2:
                    Assert.assertEquals(exception,test1.getMoneyInArea3(baggageSize,baggageWeight));
                    break;
                case 3:
                    Assert.assertEquals(exception,test1.getMoneyInArea4(baggageSize,baggageWeight));
                    break;
                case 4:
                    Assert.assertEquals(exception,test1.getMoneyInArea5(baggageSize,baggageWeight));
                    break;
                default:break;
            }
        }
        System.out.println("函数getMoneyInArea1-getMoneyInArea1测试结束");
    }

    @Test(testName = "函数getSpecialCost测试")
    public void whiteTest6() throws IOException {
        System.out.println("函数getSpecialCost测试开始");
        FileInputStream f=new FileInputStream("src/main/resources/static/黑盒测试用例1.xlsx");
        XSSFWorkbook wb=new XSSFWorkbook(f);
        XSSFSheet sheet=wb.getSheetAt(3);
        internationalExcessCost test1=new internationalExcessCost();
        String baggageType="";
        int baggageWeight=0;
        int exception=0;
        for(int i=1;i<16;i++){
            XSSFRow row=sheet.getRow(i);
            for(int j=0;j<row.getLastCellNum();j++){
                if(j==0)System.out.print('[');
                System.out.print(row.getCell(j));
                if(j<row.getLastCellNum()-1)System.out.print(',');
            }
            System.out.println(']');
            XSSFCell tes=row.getCell(0);
            tes.setCellType(CellType.STRING);
            baggageType=tes.toString();
            tes=row.getCell(1);
            tes.setCellType(CellType.STRING);
            baggageWeight=Integer.valueOf(tes.toString());
            tes=row.getCell(2);
            tes.setCellType(CellType.STRING);
            exception=Integer.valueOf(tes.toString());
            Assert.assertEquals(exception,test1.getSpecialCost(baggageType,baggageWeight));
        }
        System.out.println("函数getSpecialCost测试结束");
    }

//        System.out.println("test");
}
