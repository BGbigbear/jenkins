package com.example.springboot;

public class DomesticPassenger {
    String ticketType;
    String member;
    String shippingSpace;
    int price;

    public String getTicketType(){return this.ticketType;};

    public void setTicketType(String ticketType){this.ticketType=ticketType;};

    public String getMember(){return this.member;};

    public void setMember(String member){this.member=member;};

    public String getShippingSpace(){return this.shippingSpace;};

    public void setShippingSpace(String shippingSpace){this.shippingSpace=shippingSpace;};

    public int getPrice(){return this.price;};

    public void setPrice(int price){this.price=price;};

}
