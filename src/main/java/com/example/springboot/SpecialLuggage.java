package com.example.springboot;

public class SpecialLuggage {
    int weight;
    String luggageType;

    public int getWeight(){
            return weight;
        }

    public void setWeight(int weight){
            this.weight = weight;
        }

    public String getLuggageType(){
        return luggageType;
    }

    public void setLuggageType(String LuggageType){this.luggageType = LuggageType;}
}
