package com.example.springboot;


public class OrdinaryLuggage {
    int len;
    int width;
    int height;
    int weight;
    String airLine;

    public int getLen(){
        return len;
    }

    public void setLen(int len){
        this.len = len;
    }

    public int getWidth(){
        return width;
    }

    public void setWidth(int width){
        this.width = width;
    }

    public int getWeight(){
        return weight;
    }

    public void setWeight(int weight){
        this.weight = weight;
    }

    public int getHeight(){
        return height;
    }

    public void setHeight(int heigth){
        this.height = heigth;
    }

    public String getAirLine(){
        return airLine;
    }

    public void setAirLine(String airLine){
        this.airLine = airLine;
    }
}


